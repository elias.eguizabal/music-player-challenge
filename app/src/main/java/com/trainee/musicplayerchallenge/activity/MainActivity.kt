package com.trainee.musicplayerchallenge.activity

import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.trainee.musicplayerchallenge.R
import com.trainee.musicplayerchallenge.databinding.ActivityMainBinding
import com.trainee.musicplayerchallenge.service.MediaPlaybackService
import com.trainee.musicplayerchallenge.util.ServiceAction
import com.trainee.musicplayerchallenge.util.SongState
import com.trainee.musicplayerchallenge.util.msToTime

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding
    private val songsResources = arrayListOf(R.raw.boomerang, R.raw.gorobot)
    lateinit var mService: MediaPlaybackService
    private var mBound: Boolean = false
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as MediaPlaybackService.MyBinder
            mService = binder.getService()
            mBound = true
            Log.d("Live Cycle","Bind")
            mService.setSongsList(songs)
            mService.setData()
            Log.d("Live Cycle","ListSongSet")
            observersFromService()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }
    private val songs = arrayListOf<Uri>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        val view = mainBinding.root
        setContentView(view)

        //setSongsList()
        setSongsList()
        //mainViewModel.setMetaData(this)
        listeners()
    }

    private fun listeners() {
        Log.d("Live Cycle","Listeners")
        mainBinding.btnPlay.setOnClickListener {
            managePlayMusic()
            mainBinding.btnPlay.visibility = View.INVISIBLE
            mainBinding.btnPause.visibility = View.VISIBLE
        }

        mainBinding.btnPause.setOnClickListener {
            pauseSong()
            mainBinding.btnPlay.visibility = View.VISIBLE
            mainBinding.btnPause.visibility = View.INVISIBLE
        }

        mainBinding.btnNext.setOnClickListener {
            nextSong()
        }

        mainBinding.btnBack.setOnClickListener {
            prevSong()
        }

        mainBinding.btnInfoSong.setOnClickListener {
            Intent(this, DetailsActivity::class.java).run {
                this.putExtra("song", mService.getActualSongData())
                this.putExtra("albumPic", mService.getAlbumBitArray())
                startActivity(this)
            }
        }

        mainBinding.seekBarSong.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    mService.setSeekBare(progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }

    private fun setSongsList() {
        Log.d("Live Cycle","SetSongList")
        songsResources.forEach {
            songs.add(
                Uri.parse(
                    ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                            resources.getResourcePackageName(it) +
                            '/' + resources.getResourceTypeName(it) +
                            '/' + resources.getResourceEntryName(it)
                )
            )
        }
    }

    private fun assignMetaData() {
        Log.d("Live Cycle","initAssigMetaData")
        mainBinding.tvSongName.text = mService.getActualSongData().title
        mainBinding.tvBandNameMain.text = mService.getActualSongData().author
        mainBinding.tvTotalTime.text = mService.getActualSongData().duration?.msToTime()
        mainBinding.imageDisc.setImageBitmap(mService.getAlbumBitMap())
        Log.d("Live Cycle","completeAssigMetaData")
    }

    private fun managePlayMusic() {
        if (mService.getSongState() == SongState.NOT_INIT) {
            startServiceMusic()
        } else if (mService.getSongState() == SongState.PAUSED) {
            playSong()
        }
    }

    private fun startServiceMusic() {
        val startIntent = Intent(this, MediaPlaybackService::class.java)
        startIntent.action = ServiceAction.MAIN_ACTION.action
        startService(startIntent)
        playSong()
    }

    private fun playSong() {
        mService.playAction()
    }

    private fun pauseSong() {
        mService.pauseAction()
    }

    private fun nextSong() {
        if (mService.getSongState() == SongState.NOT_INIT) {
            startServiceMusic()
        } else {
            mService.nextSongAction()
        }
    }

    private fun prevSong() {
        if (mService.getSongState() == SongState.NOT_INIT) {
            startServiceMusic()
        } else {
            mService.prevSongAction()
        }
    }

    override fun onStart() {
        super.onStart()
        Intent(this, MediaPlaybackService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
        mBound = false
    }

    private fun observersFromService() {
        Log.d("Live Cycle","Observers")
        mService.progressLiveData.observe(this, {
            if (mService.serviceState.value == SongState.PLAYING) {
                mainBinding.seekBarSong.progress = it
                mainBinding.tvActualTime.text = it.msToTime()
            }
        })

        mService.serviceState.observe(this, { state ->
            when (state) {
                SongState.PLAYING -> {
//                    mainViewModel.currentSong = mService.getCurrentSong()
//                    mainViewModel.setMetaData(this)
                    assignMetaData()
                    mainBinding.seekBarSong.max = mService.getDuration()
                    mainBinding.tvTotalTime.text = mService.getDuration().msToTime()
                    mainBinding.btnPlay.visibility = View.INVISIBLE
                    mainBinding.btnPause.visibility = View.VISIBLE
                }
                SongState.PAUSED -> {
                    mainBinding.btnPlay.visibility = View.VISIBLE
                    mainBinding.btnPause.visibility = View.INVISIBLE
                }
                else -> {
                    mainBinding.btnPlay.visibility = View.VISIBLE
                    mainBinding.btnPause.visibility = View.INVISIBLE
                }
            }
        })

        mService.metaDataSet.observe(this,{
            if (it){
                assignMetaData()
            }
        })
    }

}
