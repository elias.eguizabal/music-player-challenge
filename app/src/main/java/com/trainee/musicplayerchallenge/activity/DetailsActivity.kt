package com.trainee.musicplayerchallenge.activity

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.trainee.musicplayerchallenge.databinding.ActivityDetailsBinding
import com.trainee.musicplayerchallenge.model.Song
import com.trainee.musicplayerchallenge.util.msToTime

class DetailsActivity : AppCompatActivity() {

    private lateinit var detailsBinding: ActivityDetailsBinding
    private lateinit var songData: Song
    private lateinit var albumPic: ByteArray

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailsBinding = ActivityDetailsBinding.inflate(layoutInflater)
        val view = detailsBinding.root
        setContentView(view)

        intent.extras?.apply {
            songData = this.getSerializable("song") as Song
            albumPic = this.getByteArray("albumPic") as ByteArray
            assignData()
        }
    }

    private fun assignData() {
        detailsBinding.tvSongTitle.text = songData.title
        detailsBinding.tvBandName.text = songData.author
        detailsBinding.tvAlbum.text = songData.album
        detailsBinding.tvDuration.text = songData.duration?.msToTime()
        detailsBinding.tvBitRate.text = songData.bitRate?.div(1000).toString()
        detailsBinding.tvGender.text = songData.gender
        detailsBinding.tvYear.text = songData.year

        if(albumPic.isNotEmpty()){
            detailsBinding.ivDic.setImageBitmap(BitmapFactory.decodeByteArray(albumPic, 0, albumPic.size))
        }
    }

}