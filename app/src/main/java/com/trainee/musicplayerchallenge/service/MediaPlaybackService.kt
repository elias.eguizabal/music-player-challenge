package com.trainee.musicplayerchallenge.service

import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.*
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.trainee.musicplayerchallenge.R
import com.trainee.musicplayerchallenge.model.Song
import com.trainee.musicplayerchallenge.util.ServiceAction
import com.trainee.musicplayerchallenge.util.SongState

class MediaPlaybackService : Service(), MediaPlayer.OnPreparedListener {

    private lateinit var mPlayer: MediaPlayer
    private lateinit var mNotificationManager: NotificationManager
    private lateinit var notificationManagerClass: NotificationManagerClass
    private lateinit var runnable: Runnable
    private var mHandler: Handler = Handler(Looper.getMainLooper())
    private val songs = arrayListOf<Uri>()
    private var currentSong = 0
    private val mBinder = MyBinder()
    val progressLiveData: MutableLiveData<Int> = MutableLiveData()
    val serviceState: MutableLiveData<SongState> = MutableLiveData()
    val metaDataSet: MutableLiveData<Boolean> = MutableLiveData()
    private val metaData = MediaMetadataRetriever()
    private var actualSoundData: Song = Song()
    private var albumBytes: ByteArray? = byteArrayOf()
    private lateinit var albumBitMap: Bitmap
    var state = SongState.NOT_INIT

    companion object {

        const val NOTIFICATION_ID_FOREGROUND_SERVICE = 8466503
    }

    override fun onBind(arg0: Intent): IBinder {
        return mBinder
    }

    fun setData() {
        setMetaData(this)
    }

    inner class MyBinder : Binder() {
        fun getService(): MediaPlaybackService = this@MediaPlaybackService
    }

    override fun onCreate() {
        super.onCreate()
        state = SongState.NOT_INIT
        serviceState.value = SongState.NOT_INIT
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManagerClass = NotificationManagerClass(this, mNotificationManager, packageName)
        initPlayer()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent.action == null) {
            stopForeground(true)
            stopSelf()
            return START_NOT_STICKY
        }
        when (intent.action) {
            ServiceAction.PAUSE_ACTION.action -> {
                pauseAction()
            }
            ServiceAction.PLAY_ACTION.action -> {
                playAction()
            }
            ServiceAction.NEXT_ACTION.action -> {
                nextSongAction()
            }
            ServiceAction.PREV_ACTION.action -> {
                prevSongAction()
            }
            ServiceAction.STOP_ACTION.action -> {
                stopAction()
            }
        }
        return START_NOT_STICKY
    }

    private fun updateNotification(newState: SongState) {
        setMetaData(this)
        mNotificationManager.notify(
            NOTIFICATION_ID_FOREGROUND_SERVICE,
            notificationManagerClass.getUpdateNotification(newState, actualSoundData, albumBitMap)
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        stopAction()
        stopForeground(true)
        stopSelf()
    }

    fun playAction() {
        if (state == SongState.NOT_INIT) {
            state = SongState.PREPARE
            serviceState.value = SongState.PREPARE
            updateNotification(state)
            initPlayer()
            play()
        } else if (state == SongState.PAUSED) {
            state = SongState.PLAYING
            serviceState.value = SongState.PLAYING
            updateNotification(state)
            resumePlayer()
        }
    }

    fun pauseAction() {
        state = SongState.PAUSED
        serviceState.value = SongState.PAUSED
        updateNotification(state)
        pausePlayer()
    }

    private fun stopAction() {
        mPlayer.stop()
        mPlayer.reset()
        mPlayer.release()
        state = SongState.NOT_INIT
        serviceState.value = SongState.NOT_INIT
        mHandler.removeCallbacks(runnable)
        mNotificationManager.cancelAll()
        stopSelf()
    }

    private fun pausePlayer() {
        mPlayer.pause()
    }

    private fun resumePlayer() {
        mPlayer.start()
    }

    private fun initPlayer() {
        mPlayer = MediaPlayer()
        mPlayer.setOnPreparedListener(this)
        startPretendLongRunningTask()
    }

    private fun play() {
        mPlayer.reset()
        mPlayer.setDataSource(this, songs[currentSong])
        mPlayer.prepareAsync()
    }

    fun nextSongAction() {
        if (currentSong == songs.lastIndex) {
            currentSong = 0
        } else {
            currentSong += 1
        }
        mPlayer.reset()
        mPlayer.setDataSource(this, songs[currentSong])
        mPlayer.prepareAsync()
    }

    fun prevSongAction() {
        if (currentSong == 0) {
            currentSong = songs.lastIndex
        } else {
            currentSong -= 1
        }
        mPlayer.reset()
        mPlayer.setDataSource(this, songs[currentSong])
        mPlayer.prepareAsync()
    }

    override fun onPrepared(mp: MediaPlayer) {
        state = SongState.PLAYING
        serviceState.value = SongState.PLAYING
        mPlayer.setWakeMode(this, PowerManager.PARTIAL_WAKE_LOCK)
        mPlayer.start()
        updateNotification(state)
    }

    fun setSongsList(listSong: ArrayList<Uri>) {
        songs.clear()
        songs.addAll(listSong)
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        stopSelf()
    }

    private fun startPretendLongRunningTask() {
        runnable = Runnable {
            progressLiveData.postValue(mPlayer.currentPosition)
            mHandler.postDelayed(runnable, 1000)
        }
        mHandler.postDelayed(runnable, 1000)
    }

    fun getDuration() = mPlayer.duration

    fun setSeekBare(ms: Int) {
        mPlayer.seekTo(ms)
    }

    fun getSongState() = state

    fun getActualSongData() = actualSoundData

    fun getAlbumBitArray() = albumBytes

    fun getAlbumBitMap() = albumBitMap

    private fun setMetaData(context: Context) {
        Log.d("Live Cycle", "initAssigMetaData Service")
        metaData.setDataSource(context, songs[currentSong])
        actualSoundData = Song(
            metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE),
            metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST),
            metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM),
            metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)?.toInt(),
            metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE),
            metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE)?.toInt(),
            metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR)
        )
        albumBytes = metaData.embeddedPicture
        albumBitMap = albumBytes?.size?.let { BitmapFactory.decodeByteArray(albumBytes, 0, it) }
            ?: kotlin.run {
                BitmapFactory.decodeResource(resources, R.drawable.ic_disc)
            }

        metaDataSet.value = true
    }


}
