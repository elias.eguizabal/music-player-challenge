package com.trainee.musicplayerchallenge.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.trainee.musicplayerchallenge.R
import com.trainee.musicplayerchallenge.activity.MainActivity
import com.trainee.musicplayerchallenge.model.Song
import com.trainee.musicplayerchallenge.util.ServiceAction
import com.trainee.musicplayerchallenge.util.SongState

class NotificationManagerClass(
    private val context: Context,
    mNotificationManager: NotificationManager?,
    packageName: String
) {

    companion object {
        private const val FOREGROUND_CHANNEL_ID = "foreground_channel_id"
    }

    private val lRemoteViews = RemoteViews(packageName, R.layout.music_notification_normal)
    private val lRemoteViewsLarge = RemoteViews(packageName, R.layout.music_notification_expanded)
    private var lPendingPlayIntent: PendingIntent
    private var lPendingPauseIntent: PendingIntent
    private var lPendingNextIntent: PendingIntent
    private var lPendingPrevIntent: PendingIntent
    private var pendingIntent: PendingIntent

    init {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
            mNotificationManager!!.getNotificationChannel(FOREGROUND_CHANNEL_ID) == null
        ) {
            val name: CharSequence = "Music Player Service"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(FOREGROUND_CHANNEL_ID, name, importance)
            mChannel.setSound(null, null)
            mChannel.enableVibration(false)
            mNotificationManager.createNotificationChannel(mChannel)
        }
        val notificationIntent = Intent(context, MainActivity::class.java)
        notificationIntent.action = ServiceAction.MAIN_ACTION.action

        pendingIntent = PendingIntent.getActivity(
            context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        val lPauseIntent = Intent(context, MediaPlaybackService::class.java)
        lPauseIntent.action = ServiceAction.PAUSE_ACTION.action
        lPendingPauseIntent =
            PendingIntent.getService(context, 0, lPauseIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val playIntent = Intent(context, MediaPlaybackService::class.java)
        playIntent.action = ServiceAction.PLAY_ACTION.action
        lPendingPlayIntent =
            PendingIntent.getService(context, 0, playIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val lNextIntent = Intent(context, MediaPlaybackService::class.java)
        lNextIntent.action = ServiceAction.NEXT_ACTION.action
        lPendingNextIntent =
            PendingIntent.getService(context, 0, lNextIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val lPrevIntent = Intent(context, MediaPlaybackService::class.java)
        lPrevIntent.action = ServiceAction.NEXT_ACTION.action
        lPendingPrevIntent =
            PendingIntent.getService(context, 0, lPrevIntent, PendingIntent.FLAG_UPDATE_CURRENT)

    }


    private fun updateState(state: SongState, song: Song, bitmap: Bitmap):Notification {

        when (state) {
            SongState.PAUSED -> {
                Log.d("MediaPlaybackService", "Waiting")
                waitingRemoteView(song,bitmap)
                return buildNotification(false)
            }
            SongState.PLAYING -> {
                playingRemoteView(song,bitmap)
                return buildNotification(true)
            }
            SongState.PREPARE -> {
                playingRemoteView(song,bitmap)
                return buildNotification(true)
            }
            SongState.NOT_INIT -> {
                Log.d("MediaPlaybackService", "Waiting")
                waitingRemoteView(song,bitmap)
                return buildNotification(false)
            }
        }
    }

    private fun playingRemoteView(song: Song,bitmap: Bitmap) {
        lRemoteViews.setOnClickPendingIntent(R.id.ib_notification_pause, lPendingPauseIntent)
        lRemoteViews.setOnClickPendingIntent(R.id.ib_notification_next, lPendingNextIntent)
        lRemoteViews.setOnClickPendingIntent(R.id.ib_notification_prev, lPendingPrevIntent)
        lRemoteViews.setViewVisibility(R.id.ib_notification_play, View.GONE)
        lRemoteViews.setViewVisibility(R.id.ib_notification_pause, View.VISIBLE)
        lRemoteViews.setTextViewText(R.id.tv_notification_bandName,song.author)
        lRemoteViews.setTextViewText(R.id.tv_notification_songName,song.title)
        lRemoteViews.setImageViewBitmap(R.id.iv_notification_album,bitmap)

        lRemoteViewsLarge.setOnClickPendingIntent(R.id.ib_notification_next_exp, lPendingNextIntent)
        lRemoteViewsLarge.setOnClickPendingIntent(
            R.id.ib_notification_prev_exp,
            lPendingPrevIntent
        )
        lRemoteViewsLarge.setOnClickPendingIntent(
            R.id.ib_notification_pause_exp,
            lPendingPauseIntent
        )
        lRemoteViewsLarge.setViewVisibility(R.id.ib_notification_play_exp, View.GONE)
        lRemoteViewsLarge.setViewVisibility(R.id.ib_notification_pause_exp, View.VISIBLE)
        lRemoteViewsLarge.setTextViewText(R.id.tv_notification_bandName_exp,song.author)
        lRemoteViewsLarge.setTextViewText(R.id.tv_notification_songName_exp,song.title)
        lRemoteViewsLarge.setImageViewBitmap(R.id.iv_notification_album_exp,bitmap)
    }

    private fun waitingRemoteView(song: Song,bitmap: Bitmap) {
        lRemoteViews.setOnClickPendingIntent(R.id.ib_notification_play, lPendingPlayIntent)
        lRemoteViews.setViewVisibility(R.id.ib_notification_pause, View.GONE)
        lRemoteViews.setViewVisibility(R.id.ib_notification_play, View.VISIBLE)
        lRemoteViews.setTextViewText(R.id.tv_notification_bandName,song.author)
        lRemoteViews.setTextViewText(R.id.tv_notification_songName,song.title)
        lRemoteViews.setImageViewBitmap(R.id.iv_notification_album,bitmap)

        lRemoteViewsLarge.setOnClickPendingIntent(R.id.ib_notification_play_exp, lPendingPlayIntent)
        lRemoteViewsLarge.setViewVisibility(R.id.ib_notification_pause_exp, View.GONE)
        lRemoteViewsLarge.setViewVisibility(R.id.ib_notification_play_exp, View.VISIBLE)
        lRemoteViewsLarge.setTextViewText(R.id.tv_notification_bandName_exp,song.author)
        lRemoteViewsLarge.setTextViewText(R.id.tv_notification_songName_exp,song.title)
        lRemoteViewsLarge.setImageViewBitmap(R.id.iv_notification_album_exp,bitmap)
    }

    private fun buildNotification(onGoing:Boolean): Notification {
        val lNotificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(context, FOREGROUND_CHANNEL_ID)
        lNotificationBuilder
            .setCustomContentView(lRemoteViews)
            .setCustomBigContentView(lRemoteViewsLarge)
            .setSmallIcon(R.drawable.ic_notification_music_note_24)
            .setCategory(NotificationCompat.EXTRA_MEDIA_SESSION)
            .setOnlyAlertOnce(true)
            .setOngoing(onGoing)
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            lNotificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        }

        return lNotificationBuilder.build()
    }

    fun getUpdateNotification(state: SongState,song: Song,bitmap: Bitmap): Notification {
        return updateState(state,song,bitmap)
    }

}