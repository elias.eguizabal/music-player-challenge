package com.trainee.musicplayerchallenge.model

import java.io.Serializable

data class Song(
    val title: String? = "Not available",
    val author: String? = "Not available",
    val album: String? = "Not available",
    val duration: Int? = 0,
    val gender: String? = "Not available",
    val bitRate: Int? = 0,
    val year: String? = "Not available"
) : Serializable
