package com.trainee.musicplayerchallenge.util

enum class SongState {
    PLAYING,
    PAUSED,
    PREPARE,
    NOT_INIT
}