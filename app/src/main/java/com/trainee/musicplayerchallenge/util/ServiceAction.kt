package com.trainee.musicplayerchallenge.util

enum class ServiceAction(val action: String) {
    MAIN_ACTION("music.action.main"),
    PAUSE_ACTION("music.action.pause"),
    PLAY_ACTION("music.action.play"),
    STOP_ACTION("music.action.stop"),
    PREV_ACTION("music.action.prev"),
    NEXT_ACTION("music.action.next")
}