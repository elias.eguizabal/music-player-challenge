package com.trainee.musicplayerchallenge.util

fun Int.msToTime():String{
    val segTotal = this.div(1000)
    val min = segTotal.div(60)
    val seg = segTotal.minus(60.times(min))
    return "$min:${if(seg<10) "0$seg" else "$seg"}"
}